#pragma once

#include "constants.hpp"
#include "jngl.hpp"

#include <box2d/box2d.h>

void DrawShape(jngl::Vec2 pos, b2Shape* shape);

void DrawShape(b2Body* body);
