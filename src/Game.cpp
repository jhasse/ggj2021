#include "Game.hpp"

#include "constants.hpp"
#include "engine/Fade.hpp"
#include "engine/helper.hpp"
#include "globals.hpp"
#include "Level.hpp"
#include "LevelEditor.hpp"
#include "Player.hpp"
#include "PauseMenu.hpp"
#include "WinningScreen.hpp"
#include "ColorSwitch.hpp"
#include "BorderArea.hpp"

#include <algorithm>
#include <cmath>
#include <map>
#include <fmt/core.h>

using jngl::Vec2;

Game::Game(int levelNr)
: world({ 0, 0 }), levelNr(levelNr),
  intro(levelNr > 9 ? introFontSmall : introFont,
        levelNr == 0 ? "LILA" : fmt::format("LEVEL {}", levelNr)) {
	g_world = &world;

	// hier und nicht in der initlist, da der ctor von Level Funktionen von Game (insbesondere
	// addTriangle) aufruft und dafür Game schon initialisiert sein muss:
	level = std::make_unique<Level>(*this, levelNr);
	level->spawnFromEditorObjects();

	world.SetContactListener(&contactListener);

	intro.setCenter(0, -10);
}

Game::~Game() {
	world.SetContactListener(nullptr);
	gameObjects.clear();
}

void Game::step() {
	world.Step((level_won ? 0.3f : 1.f) / float(jngl::getStepsPerSecond()), 8, 3);

	// TODO: create or destroy joints here
#ifdef NDEBUG
	stepCamera(false);
#else
	stepCamera(true);
#endif
	bool isActivated = lastColorSwitch;
	for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
		if ((*it)->step()) {
			it = gameObjects.erase(it);
			if (it == gameObjects.end()) {
				break;
			}
		} else {
			const auto a = dynamic_cast<Goal*>(it->get());
			if (a) {
				if (a->getNumPlayer() >= 2) {
					if (!level_won) {
						jngl::setWork(std::make_shared<WinningScreen>(jngl::getWork()));
						// jngl::play("sfx/win_song.ogg");
					}
					level_won = true;
					for (auto& gameObject : gameObjects) {
						if (const auto player = dynamic_cast<Player*>(gameObject.get())) {
							player->won = true;
						}
					}
				}
			}
			const auto colorSwitch = dynamic_cast<ColorSwitch*> (it->get());
			if(colorSwitch) {
				  isActivated = colorSwitch->isActivated();
			}

		}
	}
	if(isActivated != lastColorSwitch) {
		std::vector<BorderArea>* areas = level->getBorderAreas();
		for (auto& area : *areas) {
			area.switchRedBlue();
			for (auto& gameObject : gameObjects) {
				const auto player = dynamic_cast<Player*>(gameObject.get());
				if (player) {
					while (area.contains(player->getPosition())) {
						player->setPosition(player->getPosition() + jngl::Vec2(0, -10));
					}
				}
			}
		}
		lastColorSwitch = isActivated;
		triangulateBorder();
	}

	for (auto it = animations.begin(); it != animations.end(); ++it) {
		if ((*it)->step()) {
			it = animations.erase(it);
			if (it == animations.end()) {
				break;
			}
		}
	}

#ifndef NDEBUG
	if (jngl::keyPressed(jngl::key::F1)) {
		g_showBoundingBoxes = !g_showBoundingBoxes;
	}
	if (jngl::keyPressed(jngl::key::F3)) {
		secondsPassed = 0; // Intro-Schrift wieder ausblenden
		jngl::setWork(std::make_shared<LevelEditor>(shared_from_this(), *level));
	}
	if (jngl::keyPressed(jngl::key::F5)) {
		jngl::setWork(std::make_shared<Game>(levelNr));
	}
	if (jngl::keyPressed(jngl::key::F7)) {
		g_alternativePhysics = !g_alternativePhysics;
		jngl::setTitle(fmt::format("g_alternativePhysics: {}  |  g_joints: {}",
		                           g_alternativePhysics, g_joints));
	}
	if (jngl::keyPressed(jngl::key::F8)) {
		g_joints = !g_joints;
		jngl::setTitle(fmt::format("g_alternativePhysics: {}  |  g_joints: {}",
		                           g_alternativePhysics, g_joints));
	}
#endif
	if (jngl::keyPressed(jngl::key::Escape) || jngl::keyPressed(jngl::key::F10)) {
		jngl::setWork<PauseMenu>(shared_from_this());
	}

	world.SetGravity(b2Vec2(0, g_alternativePhysics ? 32 : 40));
	secondsPassed += 1. / jngl::getStepsPerSecond();
}

void Game::draw() const {
	jngl::pushMatrix();
	applyCamera();
	jngl::setColor(30, 200, 30, 255);
	for (const auto& triangle : triangles) {
		triangle.draw();
	}

	std::multimap<double, GameObject*> orderedByZIndex;
	std::vector<const Player*> players;
	for (const auto& obj : gameObjects) {
		orderedByZIndex.emplace(obj->getZIndex(), obj.get());
		if (const auto player = dynamic_cast<Player*>(obj.get())) {
			players.emplace_back(player);
		}
	}

	for (const auto& gameObject : orderedByZIndex) {
		gameObject.second->draw();
	}

	for (const auto& animation : animations) {
		animation->draw();
	}

	if (levelNr == 0) {
		jngl::setFontColor(0, 0, 0, std::clamp((secondsPassed - 2) * 190, 0., 190.));
		hintFont.print("       use [A] [D] to move,\n[W] to jump, [S] to crouch", { -300, -110 });
		hintFont.print("            use [<] [>] to move,\n[up] to jump, [down] to crouch",
		               { 130, 75 });
	}

	jngl::popMatrix();

	if (secondsPassed > 0.5 && secondsPassed < 5) {
		if (levelNr == 0) {
			jngl::setFontColor(toColor(AreaColor::PURPLE), 4 - float(secondsPassed));
		} else {
			jngl::setFontColor(28, 28, 28, 255);
		}
		intro.draw();
	}
}

std::vector<std::array<Vec2, 3>> Game::addTriangle(const Vec2& a, const Vec2& b, const Vec2& c,
                                                   AreaColor color) {
	const Vec2 position = (a + b + c) / 3.;
	assert(pointInsideTriangle(position, { a, b, c }));
	auto data = std::array<jngl::Vec2, 3>{ a, b, c };
	triangles.emplace_back(world, data, color);
	return { data };
}

jngl::Vec2 Game::getAbsoluteMousePos() const {
	return jngl::getMousePos() / cameraZoom + cameraPosition;
}

void Game::applyCamera() const {
	jngl::scale(cameraZoom);
	jngl::translate(-1 * cameraPosition);
}

double Game::getCameraZoom() const {
	return cameraZoom;
}

Vec2 Game::getCameraSpeed() const {
	return targetCameraPosition - cameraPosition;
}

Vec2 Game::getCameraPosition() const {
	return cameraPosition;
}

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

void Game::setCameraPosition(Vec2 position, const double deadzoneFactorX,
                             const double deadzoneFactorY) {
	cameraDeadzone = position - targetCameraPosition;
	const double x = 160 * deadzoneFactorX;
	const double y = 90 * deadzoneFactorY;
	if (std::abs(cameraDeadzone.x) > x) {
		cameraDeadzone.x =
		    sgn(cameraDeadzone.x) *
		    (x + (std::abs(cameraDeadzone.x) - x) / std::exp(std::abs(cameraDeadzone.x) - x));
	}
	if (std::abs(cameraDeadzone.y) > y) {
		cameraDeadzone.y =
		    sgn(cameraDeadzone.y) *
		    (y + (std::abs(cameraDeadzone.y) - y) / std::exp(std::abs(cameraDeadzone.y) - y));
	}
	// cameraDeadzone.x = std::clamp(cameraDeadzone.x, -160. * deadzoneFactor, 160. *
	// deadzoneFactor); cameraDeadzone.y = std::clamp(cameraDeadzone.y, -90. * deadzoneFactor, 90. *
	// deadzoneFactor);
	targetCameraPosition = position - cameraDeadzone;
}

void Game::setCameraPositionImmediately(Vec2 position) {
	targetCameraPosition = cameraPosition = position;
	speedAverage = 0;
}

void Game::stepCamera(bool mouseWheelZoom) {
	const auto speed = getCameraSpeed();
	const double NUMBER_OF_FRAMES = 500;
	speedAverage *= (NUMBER_OF_FRAMES - 1) / NUMBER_OF_FRAMES;
	speedAverage += // Wir berücksichtigen die y-Kombonente stärker, aufgrund des Breitbildformats
	    boost::qvm::mag_sqr(jngl::Vec2(0.9 * speed.x, 1.6 * speed.y)) / NUMBER_OF_FRAMES;
	cameraPosition += speed / 36.0;
	if (mouseWheelZoom) {
		targetCameraExponent += jngl::getMouseWheel() * 0.1;
	} else {
		// Weniger als -log(4) darf der Zoom nicht sein, da man sonst den Rand des Backgrounds sieht
		const double FACTOR = 600.;
		speedAverage = std::min(std::pow(std::log(4) * FACTOR, 2), speedAverage);
		targetCameraExponent = -std::sqrt(speedAverage) / FACTOR;
	}
	cameraExponent += (targetCameraExponent - cameraExponent) / 17.0;
	const double newCameraZoom = std::exp(cameraExponent);
	if (mouseWheelZoom) {
		// Die Kamera-Position so berechnen, dass sich die Weltkoordinaten des Mauszeigers nicht
		// ändern: m / z + c = m / z' + c'
		cameraPosition = getAbsoluteMousePos() - jngl::getMousePos() / newCameraZoom;
	}
	cameraZoom = newCameraZoom;
}

void Game::triangulateBorder() {
	triangles.clear();
	level->triangulateBorder();
}

void Game::add(std::unique_ptr<GameObject> obj) {
	gameObjects.emplace_back(obj.release());
}

void Game::remove(GameObject* obj) {
	gameObjects.erase(
	    std::remove_if(gameObjects.begin(), gameObjects.end(),
	                   [obj](const std::shared_ptr<GameObject>& p) { return p.get() == obj; }));
}

void Game::addRemoveObjects() {
	// TODO: Die Objekten werden direkt nach gameObjects gepusht
}

void Game::onLoad() {
	// Wird z. B. aufgerufen, wenn wir vom WinningScreen wieder aktiv gesetzt wurden.
}

void Game::changeLevel(int difference) {
	jngl::setWork<Fade>(std::make_shared<Game>(levelNr + difference));
}
