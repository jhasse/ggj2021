#include "ColorSwitch.hpp"
#include "Player.hpp"

#include "engine/SkeletonDrawable.hpp"
#include "constants.hpp"
#include "globals.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>


static const int RADIUS = 25;

ColorSwitch::ColorSwitch() {
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(0, 0);
	bodyDef.type = b2_kinematicBody;
	body = g_world->CreateBody(&bodyDef);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetLinearDamping(10.f);
	body->SetGravityScale(0);

	b2CircleShape shape;
	shape.m_radius = RADIUS / PIXEL_PER_METER;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0;
	fixtureDef.restitution = 0.1f;
	fixtureDef.isSensor = true;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_NON_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef)->GetUserData().pointer =
	    reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetGravityScale(1);

	numPlayer = 0;

	spAtlas* atlas = spAtlas_createFromFile("switch.atlas", nullptr);
	assert(atlas);
	spSkeletonJson* json = spSkeletonJson_create(atlas);
	json->scale = 0.5;

	const auto skeletonData = spSkeletonJson_readSkeletonDataFile(json, "switch.json");
	assert(skeletonData);
	spSkeletonJson_dispose(json);

	spAnimationStateData* animationStateData = spAnimationStateData_create(skeletonData);
	skeleton = std::make_unique<spine::SkeletonDrawable>(skeletonData, animationStateData);
	spAnimationState_setAnimationByName(skeleton->state, 0, "switch", true);
}

ColorSwitch::~ColorSwitch() {
	body->GetWorld()->DestroyBody(body);
}

bool ColorSwitch::step() {
	skeleton->step();
	time += 1. / jngl::getStepsPerSecond();
	return false;
}

void ColorSwitch::draw() const {
	jngl::pushMatrix();
	// jngl::setSpriteColor(toColor(AreaColor::PURPLE));
	jngl::setSpriteAlpha(200);
	jngl::translate(getPosition());

	jngl::scale(1. + std::sin(5 * time) / 30.);
	skeleton->draw();

	jngl::setSpriteColor(255, 255, 255, 255);
	jngl::popMatrix();

	if (g_alternativePhysics) {
		jngl::drawCircle(getPosition(), RADIUS);
	}
}

void ColorSwitch::draw(const EditorInfo& info) const {
	GameObject::draw(info);
	jngl::print("ColorSwitch", getPosition());
}

GameObject* ColorSwitch::spawn() const {
	auto p = new ColorSwitch;
	p->setPosition(getPosition());
	return p;
}

void ColorSwitch::onContact(GameObject* other, bool foot_sensor) {
	const auto b = dynamic_cast<Player*>(other);

	/*
	if (!foot_sensor && b) {
		if(b->getColor() == AreaColor::BLUE) {
			color->setBlue(255);
		} else if (b->getColor() == AreaColor::RED){
			color->setRed(255);
		}
		activated = true;
	}
	*/
	numPlayer++;
}

void ColorSwitch::endContact(GameObject* other, bool foot_sensor) {
	numPlayer --;
}

bool ColorSwitch::isActivated() {
	return numPlayer > 0;
}

