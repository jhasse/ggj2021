#include "globals.hpp"

#include <box2d/box2d.h>

bool g_showBoundingBoxes = false;

b2World* g_world = nullptr;

b2World g_dummyWorld({ 0, 0 });

bool g_alternativePhysics = false;

bool g_joints = false;

bool g_colorblind = false;
