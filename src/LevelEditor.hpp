#pragma once

#include "Goal.hpp"

#include <array>
#include <jngl.hpp>
#include <optional>

class Game;
class Level;

class LevelEditor : public jngl::Work {
public:
	LevelEditor(std::shared_ptr<Game>, Level&);
	~LevelEditor() override;

	LevelEditor(const LevelEditor&) = delete;
	LevelEditor(LevelEditor&&) = delete;
	LevelEditor& operator=(const LevelEditor&) = delete;
	LevelEditor& operator=(LevelEditor&&) = delete;

	void step() override;
	void draw() const override;
	void onQuitEvent() override;

private:
	std::shared_ptr<Game> game;
	bool previousMouseVisibility;
	Level& level;
	std::optional<jngl::Vec2> grabMousePos;

	enum class MouseType {
		SELECT,
		ADD_BORDER_AREA,
		ADD_GOAL,
		ADD_COLOR_SWITCH,
		ADD_BOX,
	};
	MouseType mouseType = MouseType::SELECT;
	std::optional<std::array<jngl::Vec2, 2>> selectionRectangle;

	jngl::Container container;
};
