#include "ContactListener.hpp"

#include "GameObject.hpp"
#include "Goal.hpp"

void ContactListener::BeginContact(b2Contact* contact) {

	bool footSensorA = false; // check if fixture A was the foot sensor
	bool footSensorB = false;
	auto fixtureUserData_a = contact->GetFixtureA()->GetUserData();
	auto fixtureUserData_b = contact->GetFixtureB()->GetUserData();
	const auto bodyA = contact->GetFixtureA()->GetBody();
	const auto bodyB = contact->GetFixtureB()->GetBody();

	const auto a_pointer = reinterpret_cast<GameObject*> (fixtureUserData_a.pointer);
	const auto b_pointer = reinterpret_cast<GameObject*>(fixtureUserData_b.pointer);
	if(a_pointer) {
		const auto b = dynamic_cast<Goal*>(b_pointer);
		if (!b_pointer || !b) {
			a_pointer->numFootContacts++;
			footSensorA = true;
		}
	}

	// check if fixture A was the foot sensor
	if(b_pointer) {
		const auto a = dynamic_cast<Goal*>(a_pointer);
		if (!a_pointer || !a) {
			b_pointer->numFootContacts++;
			footSensorB = true;
		}
	}

	if (!bodyA || !bodyB) {
		return;
	}
	const auto a = reinterpret_cast<GameObject*>(bodyA->GetUserData().pointer);
	const auto b = reinterpret_cast<GameObject*>(bodyB->GetUserData().pointer);
	if (a) {
		a->onContact(b, footSensorB);
	}
	if (b) {
		b->onContact(a, footSensorA);
	}
}

void ContactListener::EndContact(b2Contact* contact) {

	bool footSensorA = false; // check if fixture A was the foot sensor
	bool footSensorB = false;
	auto fixtureUserData_a = contact->GetFixtureA()->GetUserData();
	auto fixtureUserData_b = contact->GetFixtureB()->GetUserData();

	const auto bodyA = contact->GetFixtureA()->GetBody();
	const auto bodyB = contact->GetFixtureB()->GetBody();

	const auto a_pointer = reinterpret_cast<GameObject*> (fixtureUserData_a.pointer);
	const auto b_pointer = reinterpret_cast<GameObject*>(fixtureUserData_b.pointer);
	if(a_pointer) {
		const auto b = dynamic_cast<Goal*>(b_pointer);
		if (!b_pointer || !b) {
			a_pointer->numFootContacts--;
			footSensorA = true;
		}
	}

	// check if fixture A was the foot sensor
	if(b_pointer) {
		const auto a = dynamic_cast<Goal*>(a_pointer);
		if (!a_pointer || !a) {
			b_pointer->numFootContacts--;
			footSensorB = true;
		}
	}

	if (!bodyA || !bodyB) {
		return;
	}
	const auto a = reinterpret_cast<GameObject*>(bodyA->GetUserData().pointer);
	const auto b = reinterpret_cast<GameObject*>(bodyB->GetUserData().pointer);
	if (a) {
		a->endContact(b, footSensorB);
	}
	if (b) {
		b->endContact(a, footSensorA);
	}
}
