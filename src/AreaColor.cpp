#include "AreaColor.hpp"

#include "constants.hpp"
#include "globals.hpp"

jngl::Color toColor(AreaColor c) {
	switch (c) {
	case AreaColor::RED:
		return g_colorblind ? 0xff2222_rgb : 0xdc143c_rgb;
		break;
	case AreaColor::BLUE:
		return g_colorblind ? 0x2222ff_rgb : 0x0088ff_rgb;
		break;
	case AreaColor::PURPLE:
		return g_colorblind ? 0x000000_rgb : 0x7f29d6_rgb; // Altes Lila: 0xbb22ff_rgb
		break;
	}
	return 0xbc457f_rgb;
}

uint16_t toFilter(AreaColor color) {
	switch (color) {
	case AreaColor::RED:
		return FILTER_CATEGORY_PLAYER0;
		break;
	case AreaColor::BLUE:
		return FILTER_CATEGORY_PLAYER1;
		break;
	case AreaColor::PURPLE:
		return 0;
	}
	return 0;
}
