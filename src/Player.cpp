#include "Player.hpp"

#include "engine/AnimationState.hpp"
#include "engine/SkeletonDrawable.hpp"
#include "AreaColor.hpp"
#include "DebugDraw.hpp"
#include "Gamepad.hpp"
#include "Keyboard.hpp"
#include "globals.hpp"
#include "BorderArea.hpp"

#include <cmath>
#include <spine/spine.h>

void animationStateListener(spAnimationState* state, spEventType type, spTrackEntry* entry,
                            spEvent* event) {
	switch (type) {
	case SP_ANIMATION_INTERRUPT:
		break;

	case SP_ANIMATION_COMPLETE:
		if (!entry->loop) {
			reinterpret_cast<Player*>(state->userData)->onAnimationComplete();
		}
		break;
	default:
		break;
	}
}

Player::Player(b2World* world, const int playerNr)
: playerNr(playerNr), color(playerNr == 0 ? AreaColor::RED : AreaColor::BLUE) {
	if (!world) {
		world = &g_dummyWorld;
	}
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	// bodyDef.fixedRotation = true;
	body = world->CreateBody(&bodyDef);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetLinearDamping(10.f);

	createFixtureAndShape();

	const auto controllers = jngl::getConnectedControllers();
	if (controllers.size() > playerNr) {
		control = std::make_unique<Gamepad>(controllers[playerNr], playerNr);
	} else {
		control = std::make_unique<Keyboard>(playerNr);
	}

    numFootContacts = 0;

	spAtlas* atlas = spAtlas_createFromFile("lila_player.atlas", nullptr);
	assert(atlas);
	spSkeletonJson* json = spSkeletonJson_create(atlas);
	// json->scale = 0.06;

	const auto skeletonData = spSkeletonJson_readSkeletonDataFile(json, "lila_player.json");
	assert(skeletonData);
	spSkeletonJson_dispose(json);

	spAnimationStateData* animationStateData = spAnimationStateData_create(skeletonData);
	skeleton = std::make_unique<spine::SkeletonDrawable>(skeletonData, animationStateData);
	skeleton->state->userData = this;
	skeleton->state->listener = animationStateListener;
	spAnimationState_setAnimationByName(skeleton->state, 0, "idle", true);
	// spSkeleton_setSkinByName(skeleton->skeleton, "blue");

	// spSkeleton_setAttachment(skeleton->skeleton, "head", "head");

	blinkAnimation = std::make_unique<spine::AnimationState>(*skeleton, "add_blink");

	skeleton->step();
}

Player::~Player() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

template <class T> constexpr const T& clamp(const T& v, const T& lo, const T& hi) {
	assert(!(hi < lo));
	return (v < lo) ? lo : (hi < v) ? hi : v;
}

bool Player::step() {

	if (createTheJoint)
	{
		createJoint();
	}
	if (destroyTheJoint)
	{
		destroyJoint();
	}
	createTheJoint = false;
	destroyTheJoint = false;

	if (crouch) {
		if (!control->crouch()) {
			crouch = false;
			createFixtureAndShape();
		}
	} else if (control->crouch()) {
		crouch = true;
		createFixtureAndShape();
	}

	/// Loopt eine Animation falls sie noch nicht läuft
	const auto loop = [this](const std::string& name) {
		if (currentAnimation != name) {
			currentAnimation = name;
			spAnimationState_setAnimationByName(skeleton->state, 0, name.c_str(), true);
		}
	};

	const float MAX_ANGLE = M_PI / 4.;

	skeleton->step();
	blinkAnimation->step();
	if (control->jump()) {
		// if (this->joint)
		// {
			destroyTheJoint = true;
		// }

		if (numFootContacts > 0 && jumpAvailable) {
			jumpAvailable = false;
			remainingJumpSteps = jngl::getStepsPerSecond() / (g_alternativePhysics ? 7 : 10);

			// kein Check was die aktuelle Animation ist, da wir immer überschreiben
			currentAnimation = "jump";
			spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(),
			                                    false);
			// einfach im Frame hängen bleiben. Die jump-Animation sollte lang genug sein:
			nextAnimation = "";
		}
	} else {
		--remainingJumpSteps; // wenn man Springen länger hält, soll man höher springen können
		jumpAvailable = true;
	}

	if (remainingJumpSteps > 0) {
		--remainingJumpSteps;
		float jumpFactor = 120;
		b2Vec2 massShift = b2Vec2_zero;

		if (joint1)
		{
			jumpFactor = jumpFactor * 2;
			massShift.y = (body->GetPosition() - otherPlayer->body->GetPosition()).Length() / 2;
		}

		float force = body->GetMass() * (g_alternativePhysics ? 80 : jumpFactor);
		body->ApplyForce(b2Vec2(0, -force), body->GetWorldCenter() + massShift, true);

	}

	float movementFactor = 70;
	if (numFootContacts == 0) {
		movementFactor = 30; // In der Luft kann man nicht so stark "lenken"
	}

	if (!otherPlayer)
	{
		// Wenn man nicht hockt, dann noch etwas nach oben, damit man leichter über Schrägen gehen
		// kann:
		body->ApplyForce(
		    pixelToMeter(movementFactor *
		                 (control->getMovement() + (crouch ? jngl::Vec2() : jngl::Vec2(0, -0.5)))),
		    body->GetWorldCenter(), true);

		// https://gamedev.stackexchange.com/a/63095/117183
		body->ApplyTorque(-0.01f * getRotation(), true);
	}else{
		if (!otherPlayer->joint1)
		{
			body->ApplyForce(pixelToMeter(movementFactor * (control->getMovement() + jngl::Vec2(0, -0.5))),
	                 body->GetWorldCenter(), true);

	// https://gamedev.stackexchange.com/a/63095/117183
		body->ApplyTorque(-0.01f * getRotation(), true);
		}

	}

	if (currentAnimation != "jump" || body->GetLinearVelocity().y > 0.1) {
		// ^~ die jump-Animation nicht unterbrechen, außer wir fallen
		if (stepsFalling > 4 && numFootContacts == 0) {
			if (!fallAnimationActive()) {
				currentAnimation = currentAnimation == "move" ? "move_fall" : "fall";
				spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(),
				                                    false);
				nextAnimation = "fall_loop";
			}
		} else if ((body->GetLinearVelocity().x < -1 || body->GetLinearVelocity().x > 1) &&
		           std::abs(getRotation()) < MAX_ANGLE / 2.) {
			loop(stepsFalling > 4 ? "move_fall" : "move");
		} else {
			if (currentAnimation != "land") { // "land" niemals unterbrechen
				if (stepsFalling > 4 && fallAnimationActive()) {
					currentAnimation = "land";
					spAnimationState_setAnimationByName(skeleton->state, 0,
					                                    currentAnimation.c_str(), false);
					nextAnimation = "idle";
				} else if (control->crouch()) {
					loop("charge_hold");
				} else if (currentAnimation == "charge_hold") {
					currentAnimation = "charge_jump";
					spAnimationState_setAnimationByName(skeleton->state, 0,
					                                    currentAnimation.c_str(), false);
					nextAnimation = "idle";
				} else if (currentAnimation != "charge_jump") {
					loop(won ? "happy" : "idle");
				}
			}
		}
	}

	if (body->GetLinearVelocity().y > 0.01) {
		++stepsFalling;
	} else {
		stepsFalling = 0;
	}

	time += float(90 + (rand() % 10)) / 1000.f;
	return false;
}

bool Player::fallAnimationActive() const {
	return currentAnimation == "fall" || currentAnimation == "fall_loop" ||
	       currentAnimation == "move_fall" || currentAnimation == "move_fall_loop";
}

void Player::draw() const {
	jngl::pushMatrix();
	jngl::translate(getPosition());
	jngl::rotate(getRotation() * 180 / M_PI);

	bool mirror = body->GetLinearVelocity().x > 1;
	const double xOffset = 30.8;
	jngl::translate(0, 7.9);
	jngl::setSpriteColor(toColor(color).getRed(), toColor(color).getGreen(),
	                     toColor(color).getBlue());
	if (mirror) {
		jngl::scale(-1, 1);
	}
	jngl::scale(0.06);
	skeleton->draw();
	jngl::setSpriteColor(255, 255, 255);

	jngl::popMatrix();

	if (g_showBoundingBoxes) {
		DrawShape(body);
		jngl::pushMatrix();
		jngl::translate(getPosition());
		jngl::setFontColor(0x000000_rgb);
		jngl::print(std::to_string(numFootContacts) + (playerOnTop ? " true" : "") +
		                (onAnotherPlayer ? " onAnotherPlayer" : ""),
		            playerNr == 0 ? -40 : 0, -50);
		jngl::popMatrix();
	}
}

void Player::draw(const EditorInfo& info) const {
	draw();
	GameObject::draw(info);
}

void Player::createJoint(){
	if (g_joints)
	{
		if (!otherPlayer->joint1 && !otherPlayer->joint2)
		{
			b2Vec2 pos1 = body->GetPosition();
			pos1.x = pos1.x - 0.05;
			b2Vec2 pos2 = otherPlayer->body->GetPosition();

			b2DistanceJointDef rDef;
			rDef.maxLength = ( pos1 - pos2).Length() ;//* kMaxWidth;
			rDef.stiffness = 1.0;
			rDef.damping = 0.0;
			rDef.localAnchorA = b2Vec2(0.05, 0.0);
			rDef.localAnchorB = b2Vec2_zero;
			rDef.bodyA = otherPlayer->body;
			rDef.bodyB = body;
			otherPlayer->joint1 =  (b2DistanceJoint*)this->body->GetWorld()->CreateJoint(&rDef);


			b2DistanceJointDef rDef2;
			rDef2.maxLength = ( pos1 - pos2).Length() ;//* kMaxWidth;
			rDef2.stiffness = 1.0;
			rDef2.damping = 0.0;
			rDef2.localAnchorA = b2Vec2(-0.05, 0.0);
			rDef.localAnchorB =  b2Vec2_zero;
			rDef2.bodyA = otherPlayer->body;
			rDef2.bodyB = body;
			otherPlayer->joint2 =  (b2DistanceJoint*)this->body->GetWorld()->CreateJoint(&rDef2);


			b2DistanceJointDef rDef3;
			rDef3.maxLength = ( pos1 - pos2).Length() ;//* kMaxWidth;
			rDef3.stiffness = 1.0;
			rDef3.damping = 0.0;
			rDef3.localAnchorB = b2Vec2(0.05, 0.0);
			rDef3.localAnchorA = b2Vec2_zero;
			rDef3.bodyA = otherPlayer->body;
			rDef3.bodyB = body;
			otherPlayer->joint3 =  (b2DistanceJoint*)this->body->GetWorld()->CreateJoint(&rDef3);


			b2DistanceJointDef rDef4;
			rDef4.maxLength = ( pos1 - pos2).Length() ;//* kMaxWidth;
			rDef4.stiffness = 1.0;
			rDef4.damping = 0.0;
			rDef4.localAnchorB = b2Vec2(-0.05, 0.0);
			rDef4.localAnchorA =  b2Vec2_zero;
			rDef4.bodyA = otherPlayer->body;
			rDef4.bodyB = body;
			otherPlayer->joint4 =  (b2DistanceJoint*)this->body->GetWorld()->CreateJoint(&rDef4);

			// change power


		}
	}



}

void Player::destroyJoint(){
	if (g_joints)
	{
		if (otherPlayer)if(otherPlayer->joint1 && otherPlayer->joint2)
		{
			this->body->GetWorld()->DestroyJoint(otherPlayer->joint1);
			this->body->GetWorld()->DestroyJoint(otherPlayer->joint2);
			this->body->GetWorld()->DestroyJoint(otherPlayer->joint3);
			this->body->GetWorld()->DestroyJoint(otherPlayer->joint4);
			otherPlayer->joint1 = nullptr;
			otherPlayer->joint2 = nullptr;
			otherPlayer->joint3 = nullptr;
			otherPlayer->joint4 = nullptr;

		}
	}


}


void Player::onContact(GameObject* other, bool foot_sensor) {
	if (foot_sensor && dynamic_cast<Player*>(other)) {
		otherPlayer = dynamic_cast<Player*>(other);
		otherPlayer->otherPlayer = this;
		otherPlayer->createTheJoint = true;
		playerOnTop = true;
	}
}

void Player::endContact(GameObject* other, bool foot_sensor) {
	if (foot_sensor && dynamic_cast<Player*>(other)) {
		// destroyTheJoint = true;

		playerOnTop = false;
	}
}

void Player::vibrate() {
	control->vibrate();
}

void Player::createFixtureAndShape() {
	if (b2Fixture* existingFixture = body->GetFixtureList()) {
		body->DestroyFixture(existingFixture);
	}

	b2PolygonShape shape;
	if (crouch) {
		shape.SetAsBox(9.0f / PIXEL_PER_METER, 4.0f / PIXEL_PER_METER,
		               b2Vec2(0, 2.4f / PIXEL_PER_METER), 0);
	} else {
		shape.SetAsBox(6.0f / PIXEL_PER_METER, 6.0f / PIXEL_PER_METER);
	}

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = toFilter(color);
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
}

void Player::onAnimationComplete() {
	if (nextAnimation.empty()) {
		return;
	}
	currentAnimation = nextAnimation;
	spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(), true);
}

GameObject* Player::spawn() const {
	auto p = new Player(g_world, playerNr);
	p->setPosition(getPosition());
	return p;
}


AreaColor Player::getColor() {
	return color;
}
