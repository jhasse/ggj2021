#include "helper.hpp"

#include <cmath>
#include <jngl/input.hpp>

jngl::Vec2 roundToGrid(const jngl::Vec2& vertex) {
	if (jngl::keyDown(jngl::key::ShiftL) || jngl::keyDown(jngl::key::ShiftR)) {
		return jngl::Vec2(vertex.x, vertex.y);
	}
	return { double(std::lround(vertex.x / GRID_SIZE) * GRID_SIZE),
		     double(std::lround(vertex.y / GRID_SIZE) * GRID_SIZE) };
}

bool pointInsideTriangle(const jngl::Vec2& s, const std::array<jngl::Vec2, 3>& triangle) {
	// based on https://stackoverflow.com/a/9755252
	const jngl::Vec2 a2p = s - triangle[0];
	const bool p_ab =
	    (triangle[1].x - triangle[0].x) * a2p.y - (triangle[1].y - triangle[0].y) * a2p.x > 0;
	if (((triangle[2].x - triangle[0].x) * a2p.y - (triangle[2].y - triangle[0].y) * a2p.x > 0) ==
	    p_ab) {
		return false;
	}
	if (((triangle[2].x - triangle[1].x) * (s.y - triangle[1].y) -
	         (triangle[2].y - triangle[1].y) * (s.x - triangle[1].x) >
	     0) != p_ab) {
		return false;
	}
	return true;
}
