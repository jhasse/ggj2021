#include <jngl.hpp>
#include "Keyboard.hpp"

Keyboard::Keyboard(int playerNr) : playerNr(playerNr) {
}

template <class Key> jngl::Vec2 findMovement(Key left, Key right) {
	jngl::Vec2 direction;
	if (jngl::keyDown(left)) {
		direction -= jngl::Vec2(1, 0);
	}
	if (jngl::keyDown(right)) {
		direction += jngl::Vec2(1, 0);
	}
	return direction;
}

jngl::Vec2 Keyboard::getMovement() const {
	// change speed
	jngl::Vec2 direction;
	if (playerNr == 0) {
		direction = findMovement(jngl::key::Left, jngl::key::Right);
	} else if (playerNr == 1) {
		direction = findMovement('a', 'd');
	}

	if (boost::qvm::mag_sqr(direction) > 1) {
		boost::qvm::normalize(direction);
	}
	return direction;
}

bool Keyboard::jump() const {
	if (playerNr == 0) {
		return jngl::keyDown(jngl::key::Up);
	}
	if (playerNr == 1) {
		return jngl::keyDown('w');
	}
	return false;
}

bool Keyboard::crouch() const {
	if (playerNr == 0) {
		return jngl::keyDown(jngl::key::Down);
	}
	if (playerNr == 1) {
		return jngl::keyDown('s');
	}
	return false;
}
