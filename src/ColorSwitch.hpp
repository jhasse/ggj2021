#pragma once

#include "GameObject.hpp"
#include "leveleditor/EditorObject.hpp"

#include <cereal/types/polymorphic.hpp>
#include <cereal/cereal.hpp>
#include <jngl.hpp>

class b2World;
namespace spine {
class SkeletonDrawable;
} // namespace spine

class ColorSwitch : public GameObject {
public:
	ColorSwitch();
	~ColorSwitch();

	bool step() override;
	void draw() const override;
	void draw(const EditorInfo&) const override;
	GameObject* spawn() const override;
	void onContact(GameObject* other, bool foot_sensor) override;
	void endContact(GameObject* other, bool foot_sensor) override;
	bool isActivated();

private:
	//const static float RADIUS;
	std::unique_ptr<spine::SkeletonDrawable> skeleton;
	double time = 0;

	friend class cereal::access;
	template <class Archive> void save(Archive& ar, const unsigned int) const {
		ar << getPosition();
	}
	template <class Archive> void load(Archive& ar, const unsigned int) {
		jngl::Vec2 position;
		ar >> position;
		setPosition(position);
	}

	int numPlayer;
};

CEREAL_REGISTER_TYPE(ColorSwitch)
CEREAL_REGISTER_POLYMORPHIC_RELATION(EditorObject, ColorSwitch)
