#include "EditorObject.hpp"

#include "EditorInfo.hpp"
#include "../engine/helper.hpp"

#include <jngl.hpp>

EditorObject::Action EditorObject::step(std::optional<jngl::Vec2>& cloneHere,
                                        const bool /*multipleSelection*/, jngl::Vec2 globalMousePos,
                                        EditorInfo& info) {
	info.mouseOver =
	    boost::qvm::mag_sqr(globalMousePos - getPosition()) < info.grabRadius * info.grabRadius;
	if (info.grabStartMousePos) {
		if (!jngl::mouseDown(jngl::mouse::Left)) {
			if (boost::qvm::mag_sqr(*info.grabStartMousePos - globalMousePos) < 100 &&
			    jngl::getTime() - *info.grabStartTime < 0.5) {
				// Weniger als 10px bewegt und kürzer als 100ms?
				setPosition(*info.grabStartVertexPos);
				info.selected = false; // abwählen damit nur noch der mit mouseover ausgewählt wird.
			}
			info.grabStartMousePos = std::nullopt;
			info.grabStartVertexPos = std::nullopt;
			info.grabStartTime = std::nullopt;
			return info.selected ? Action::CHANGED : Action::NEED_TO_SELECT;
		}
		setPosition(
		    roundToGrid(*info.grabStartVertexPos + (globalMousePos - *info.grabStartMousePos)));
	}
	if (info.mouseOver && jngl::keyPressed(jngl::key::Delete)) {
		return Action::DELETE_ME;
	}
	if (info.mouseOver && jngl::keyPressed(jngl::key::AltL)) {
		info.newVertex = getPosition();
	}
	if (info.newVertex) {
		info.newVertex = roundToGrid(globalMousePos);
	}
	if (info.newVertex && !jngl::keyDown(jngl::key::AltL)) {
		if (getPosition() !=
		    *info.newVertex) { // Don't add two vertices exactly on top of each other
			cloneHere = info.newVertex;
		}
		info.newVertex = {};
	}
	return Action::NOTHING;
}

void EditorObject::startDrag(EditorInfo& info, jngl::Vec2 globalMousePos) {
	info.grabStartMousePos = globalMousePos;
	info.grabStartVertexPos = getPosition();
	info.grabStartTime = jngl::getTime();
}

GameObject* EditorObject::spawn() const {
	return nullptr;
}

bool EditorObject::mouseOverGrabber(const EditorInfo& info, jngl::Vec2 pos) const {
	return boost::qvm::mag_sqr(pos - getPosition()) < info.grabRadius * info.grabRadius;
}
