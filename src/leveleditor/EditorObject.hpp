#pragma once

#include <array>
#include <cereal/cereal.hpp>
#include <jngl/Vec2.hpp>
#include <optional>

struct EditorInfo;
class GameObject;

class EditorObject {
public:
	EditorObject() = default;
	virtual ~EditorObject() = default;

	enum class Action {
		NOTHING,
		CHANGED,
		DELETE_ME,
		NEED_TO_SELECT,
	};

	/// Gibt z. B. CHANGED zurück wenn die Maustaste beim Verschieben losgelassen wird. \a cloneHere
	/// kann auf eine Position gesetzt werden, an der eine Kopie des Objekts erzeugt werden soll.
	virtual Action step(std::optional<jngl::Vec2>& cloneHere, bool multipleSelection,
	                    jngl::Vec2 globalMousePos, EditorInfo& info);

	virtual void draw(const EditorInfo&) const = 0;

	void startDrag(EditorInfo& info, jngl::Vec2 globalMousePos);

	virtual GameObject* spawn() const;

	bool mouseOverGrabber(const EditorInfo&, jngl::Vec2 pos) const;

	virtual jngl::Vec2 getPosition() const = 0;
	virtual void setPosition(jngl::Vec2) = 0;

private:
	friend class cereal::access;
	template <class Archive> void save(Archive& ar, const unsigned int) const {
		ar << cereal::make_nvp("position", getPosition());
	}
	template <class Archive> void load(Archive& ar, const unsigned int) {
		jngl::Vec2 pos;
		ar >> cereal::make_nvp("position", pos);
		setPosition(pos);
	}
};
