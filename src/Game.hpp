#pragma once

#include "AreaColor.hpp"
#include "ContactListener.hpp"
#include "Triangle.hpp"

#include <jngl.hpp>
#include <set>
#include <vector>

class Animation;
class GameObject;
class Level;
class Player;

class Game : public jngl::Work, public std::enable_shared_from_this<Game> {
public:
	Game(int levelNr);
	~Game() override;
	void onLoad() override;
	void step() override;
	void draw() const override;

	std::vector<std::array<jngl::Vec2, 3>> addTriangle(const jngl::Vec2& a, const jngl::Vec2& b,
	                                                   const jngl::Vec2& c, AreaColor);

	// Maus-Position in Welt-Koordinaten
	jngl::Vec2 getAbsoluteMousePos() const;

	/// Wendet die Kamera auf JNGLs globale ModelView-Matrix an
	void applyCamera() const;

	/// Je kleiner, desto weiter ist die Kamera herausgezoomt. 1 = default, immer größer 0
	double getCameraZoom() const;

	jngl::Vec2 getCameraPosition() const;
	jngl::Vec2 getCameraSpeed() const;
	void setCameraPosition(jngl::Vec2, double deadzoneFactorX, double deadzoneFactorY);
	void setCameraPositionImmediately(jngl::Vec2);

	void stepCamera(bool mouseWheelZoom);
	void triangulateBorder();

	void add(std::unique_ptr<GameObject>);
	void remove(GameObject*);
	void addRemoveObjects();

	// 0 = reload, -1 = vorhergies, 1 = nächstes
	void changeLevel(int difference);

private:
	b2World world;
	std::unique_ptr<Level> level;

	std::vector<std::shared_ptr<GameObject>> gameObjects;

	std::vector<std::unique_ptr<Animation>> animations;

	ContactListener contactListener;

	jngl::Vec2 cameraPosition;
	jngl::Vec2 targetCameraPosition;
	jngl::Vec2 cameraDeadzone;
	double cameraZoom = 1.0;
	double speedAverage = 0;
	double cameraExponent = 0.0;
	double targetCameraExponent = 0.0;

	std::vector<Triangle> triangles;
	bool level_won = false;

	const int levelNr;

	double secondsPassed = 0;
	jngl::Font introFont{ "Poppins-Bold.ttf", 130 };
	jngl::Font introFontSmall{ "Poppins-Bold.ttf", 119 };
	jngl::TextLine intro;

	jngl::Font hintFont{ "Poppins-Bold.ttf", 8 };

	bool lastColorSwitch = false;
};
