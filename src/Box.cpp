#include "Box.hpp"

#include "constants.hpp"
#include "globals.hpp"

#include <box2d/box2d.h>

Box::Box(b2World* world) {
	if (!world) {
		world = &g_dummyWorld;
	}
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	body = world->CreateBody(&bodyDef);

	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));

	b2PolygonShape shape;
	shape.SetAsBox(32. / PIXEL_PER_METER / 2., 32. / PIXEL_PER_METER / 2.);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 0.1f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
}

Box::~Box() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

bool Box::step() {
	return false;
}

void Box::draw() const {
	jngl::pushMatrix();
	const auto transform = body->GetTransform();
	jngl::translate(meterToPixel(transform.p));
	jngl::rotate(transform.q.GetAngle() * 180 / M_PI);
	jngl::setColor(0xcccccc_rgb);
	jngl::setAlpha(160);
	const double size = 35.8;
	jngl::drawRect(-size / 2, -size / 2, size, size);
	jngl::popMatrix();
}

void Box::draw(const EditorInfo& info) const {
	GameObject::draw(info);
	jngl::print("Box", getPosition());
}

GameObject* Box::spawn() const {
	auto p = new Box(g_world);
	p->setPosition(getPosition());
	return p;
}
