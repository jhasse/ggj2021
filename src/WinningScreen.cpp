#include "WinningScreen.hpp"

#include "Game.hpp"
#include "engine/SkeletonDrawable.hpp"

WinningScreen::WinningScreen(std::shared_ptr<jngl::Work> game) : game(std::move(game)) {
}

WinningScreen::~WinningScreen() = default;

void WinningScreen::step() {
	++time;
	game->step();
	if (time > 100) {
		std::dynamic_pointer_cast<Game>(game)->changeLevel(1);
	}
}

void WinningScreen::draw() const {
	game->draw();
	if (time < 300) {
		return;
	}
	jngl::pushMatrix();
	jngl::scale(0.55);
	jngl::translate(540, 400);
	jngl::print("WON", 0, 0);
	jngl::popMatrix();
}
