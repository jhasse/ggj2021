#pragma once

#include "gui/Button.hpp"

#include <jngl.hpp>

class PauseMenu : public jngl::Work {
public:
	explicit PauseMenu(std::shared_ptr<jngl::Work> game);
	~PauseMenu() override;

	void step() override;
	void draw() const override;

	void onQuitEvent() override;

private:
	std::shared_ptr<jngl::Work> game;
	jngl::FrameBuffer fbo;

	jngl::Container container;

	jngl::Shader fragment;
	jngl::ShaderProgram blur;

	Button colorblind;

	constexpr static double REDUCE_RESOLUTION = 4;
};
