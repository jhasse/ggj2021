#include "Gamepad.hpp"

Gamepad::Gamepad(std::shared_ptr<jngl::Controller> controller, const int playerNr)
: controller(std::move(controller)), playerNr(playerNr) {
}

jngl::Vec2 Gamepad::getMovement() const {
	const jngl::Vec2 dpad(-controller->state(jngl::controller::DpadLeft) +
	                          controller->state(jngl::controller::DpadRight),
	                      0);
	const jngl::Vec2 sticks(controller->state(jngl::controller::LeftStickX),
	                        -controller->state(jngl::controller::LeftStickY) /
	                            3. /* y axis is unused but shouldn't be completely ignored */);
	jngl::Vec2 sum = dpad + sticks;
	sum.x += abs(sum.y);
	sum.y = 0; // our return value will be applied as a force and mustn't have a vertical component
	if (boost::qvm::mag_sqr(sum) > 1) {
		return boost::qvm::normalized(sum);
	}
	return sum;
}

bool Gamepad::jump() const {
	return controller->down(jngl::controller::A);
}

bool Gamepad::crouch() const {
	return controller->down(jngl::controller::B);
}

void Gamepad::vibrate() {
	controller->rumble(0.5f, std::chrono::milliseconds(100));
}
