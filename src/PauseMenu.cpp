#include "PauseMenu.hpp"

#include "globals.hpp"
#include "Game.hpp"

#include <fstream>
#include <utility>
#include <fmt/format.h>

void replaceAll(std::string& subject, const std::string& search, const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}

std::stringstream loadAndReplace(int width, int height) {
	std::ifstream source("blur.frag");
	std::stringstream buffer;
	buffer.exceptions(std::ios_base::failbit);
	buffer << source.rdbuf();
	std::string tmp = buffer.str();
	replaceAll(tmp, "FBO_WIDTH", std::to_string(width) + ".f");
	replaceAll(tmp, "FBO_HEIGHT", std::to_string(height) + ".f");
	return std::stringstream(tmp);
}

PauseMenu::PauseMenu(std::shared_ptr<jngl::Work> game)
: game(std::move(game)),
  fbo(jngl::getWindowSize()[0] / REDUCE_RESOLUTION, jngl::getWindowSize()[1] / REDUCE_RESOLUTION),
  fragment(loadAndReplace(jngl::getWindowWidth() / REDUCE_RESOLUTION,
                          jngl::getWindowHeight() / REDUCE_RESOLUTION),
           jngl::Shader::Type::FRAGMENT),
  blur(jngl::Sprite::vertexShader(), fragment),
  colorblind(
      fmt::format("Colorblind: {}", g_colorblind ? "On" : "Off"), jngl::Vec2(0, 45),
      [this]() {
	      g_colorblind = !g_colorblind;
	      colorblind.setLabel(fmt::format("Colorblind: {}", g_colorblind ? "On" : "Off"));
      },
      true) {
	container.addWidget<Button>(
	    "Resume", jngl::Vec2(0, -135), [this]() { onQuitEvent(); }, true);
	container.addWidget<Button>("Restart Level", jngl::Vec2(0, -45), [this]() {
		dynamic_cast<Game&>(*this->game).changeLevel(0);
	 }, true);
	container.addWidget<Button>(
	    "Quit", jngl::Vec2(0, 135), [this]() { jngl::quit(); }, true);
	jngl::setMouseVisible(true);
}

PauseMenu::~PauseMenu() {
	jngl::setMouseVisible(false);
}

void PauseMenu::step() {
	container.step();
	colorblind.step();
	if (jngl::keyPressed(jngl::key::Escape)) {
		onQuitEvent();
	}
}

void PauseMenu::draw() const {
	{
		auto context = fbo.use();
		// jngl::translate(-jngl::getScreenSize() / 2. / REDUCE_RESOLUTION);
		// jngl::setColor(0x111111_rgb);
		// jngl::drawRect(-9999, -9999, 999999, 999999);
		// jngl::setColor(0xff0000_rgb);
		// jngl::drawRect(0, 0, 100, 100);
		jngl::scale(1. / REDUCE_RESOLUTION);
		game->draw();
	}
	fbo.draw(jngl::modelview().scale(REDUCE_RESOLUTION), &blur);
	container.draw();
	colorblind.draw();
}

void PauseMenu::onQuitEvent() {
	jngl::setWork(this->game);
}
